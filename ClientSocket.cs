﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using System.Threading;

namespace Noseeks
{
	class ClientSocket
	{
		private Socket mClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		private Task mConnectionTask;
		private IPAddress mIp;
		private int mPort;
		private byte[] mBuffer;
		private int mBuffSize = 2048;
		private ICommunicator mComm;

		public ClientSocket(ICommunicator activity, IPAddress ip, int port = 1800)
		{
			mComm = activity;
			mIp = ip;
			mPort = port;
			mBuffer = new byte[mBuffSize];
		}

		public void Start()
		{
			InitiateConnection();
		}

		public bool SendMessageSynchronous(string msg)
		{
			bool res = false;

			if (mConnectionTask.IsCompleted) 
			{
				byte[] buff = Encoding.ASCII.GetBytes(msg);
				mClientSocket.Send(buff);

				mClientSocket.BeginReceive (mBuffer, 0, mBuffer.Length, SocketFlags.None, new AsyncCallback (RecievedMessage), mClientSocket);
				res = true;
			}

			return res;
		}

		void RecievedMessage (IAsyncResult ar)
		{
			Socket socket = (Socket)ar.AsyncState;
			int msgRecSize;

			try
			{
				msgRecSize = socket.EndReceive(ar);
			}
			catch (SocketException)
			{
				//IDK exc is thrown.
				socket.Close();
				return;
			}

			byte[] rawMsg = new byte[msgRecSize];
			Array.Copy(mBuffer, rawMsg, msgRecSize);

			var msg = Encoding.ASCII.GetString(rawMsg);
			var info = "Message Recieved: " + msg;
			Console.WriteLine(info);
			mComm.UpdateInfoBackground(info);

			socket.BeginReceive(mBuffer, 0, mBuffer.Length, SocketFlags.None, new AsyncCallback(RecievedMessage), socket);
		}

		//Kicks of a task that loops until it connects
		private void InitiateConnection()
		{
			mConnectionTask = Task.Factory.StartNew(() =>
				{
					int attempts = 0;
					while (!mClientSocket.Connected)
					{
						try
						{
							attempts++;
							mClientSocket.Connect(mIp, mPort);
						}
						catch (SocketException)
						{
							//Console.Clear();
							//Console.WriteLine("Attempts to connect: " + attempts);
						}

					}
					Console.WriteLine("Client Connected");
				});
		}
	}
}

