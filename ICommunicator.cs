﻿using Android.Net.Wifi.P2p;
using System;
using System.Collections.Generic;

namespace Noseeks
{
	interface ICommunicator
	{
		void UpdateInfo(string data, bool immediately = true);
		void UpdateList(ICollection<WifiP2pDevice> adapter);
		void UpdateInfoBackground(string info);
	}
}