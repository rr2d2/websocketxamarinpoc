﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Views;
using System.Net;
using Android.Net.Wifi.P2p;
using System.Collections.Generic;

namespace Noseeks
{
	[Activity (Label = "Noseeks", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity, ICommunicator
	{
		private ServerSocket mServerSocket;
		private ClientSocket mClientSocket;
		private TextView mMainTxt;
		private Toast mToast;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			mMainTxt = FindViewById<TextView>(Resource.Id.txtViewMain);

			SetupButtons ();
		}

		private void ButtonClicked(object sender, EventArgs e)
		{
			var btn = sender as View;
			switch (btn.Id) {
			case Resource.Id.btnClient:
				ClientStuff ();
				break;
			case Resource.Id.btnSend:
				SendMessage ();
				break;
			case Resource.Id.btnServer:
				ServerStuff ();
				break;
			default:
				break;
			}
		}

		void ClientStuff ()
		{
			if (mClientSocket == null) 
			{
				var ipStr = FindViewById<TextView> (Resource.Id.txtInput);
				var btnClient = FindViewById<Button>(Resource.Id.btnClient);
				var btnServer = FindViewById<Button>(Resource.Id.btnServer);

				IPAddress ipAddress = Dns.GetHostEntry (ipStr.Text).AddressList [0];
				mClientSocket = new ClientSocket (this, ipAddress, 1800);
				mClientSocket.Start ();

				UpdateInfo("You are the client");
				ipStr.Text = "";
				btnClient.Enabled = false;
				btnServer.Enabled = false;
			}
		}

		void SendMessage ()
		{
			if (mClientSocket != null) 
			{
				UpdateInfo("Client sending a ping to server");
				mClientSocket.SendMessageSynchronous ("ping");
			}
		}

		void ServerStuff ()
		{
			if (mServerSocket == null) 
			{
				var ipStr = FindViewById<TextView>(Resource.Id.txtInput);
				var btnServer = FindViewById<Button>(Resource.Id.btnServer);
				var btnClient = FindViewById<Button>(Resource.Id.btnClient);

				mServerSocket = new ServerSocket (this);
				mServerSocket.Start ();

				UpdateInfo("You are the Server");
				ipStr.Text = "";
				btnServer.Enabled = false;
				btnClient.Enabled = false;
			}
		}

		void SetupButtons ()
		{
			// Get our button from the layout resource,
			// and attach an event to it
			Button btnClient = FindViewById<Button> (Resource.Id.btnClient);
			Button btnServer = FindViewById<Button> (Resource.Id.btnServer);
			Button btnPing = FindViewById<Button> (Resource.Id.btnSend);
			btnClient.Click += ButtonClicked;
			btnServer.Click += ButtonClicked;
			btnPing.Click += ButtonClicked;
		}

		public void UpdateInfo(string data, bool immediately = true)
		{
			if (mMainTxt == null)
			{
				mMainTxt = FindViewById<TextView>(Resource.Id.txtViewMain);
			}

			mMainTxt.Text += System.Environment.NewLine + data;

			//essentially shows toast even if there is a current toast that hasn't finished yet
			if (mToast != null && immediately)
			{
				mToast.Cancel();
			}

			mToast = Toast.MakeText(ApplicationContext, data, ToastLength.Short);
			mToast.Show();
		}

		public void UpdateList(ICollection<WifiP2pDevice> adapter)
		{
			UpdateInfo("Update List is not implemented");
		}

		public void UpdateInfoBackground(string info)
		{
			RunOnUiThread(() =>
			{
				UpdateInfo(info);
			});
		}
	}
}


